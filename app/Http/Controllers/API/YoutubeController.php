<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Youtube\StoreYoutubeRequest;
use App\Models\Youtube;
use Illuminate\Http\Request;

class YoutubeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Youtube\StoreYoutubeRequest
     * @return \Illuminate\Http\Response
     */
    public function store(StoreYoutubeRequest $request)
    {
        try {
            $yt = new Youtube;
            $yt->user_id = $request->get('user_id');
            $yt->title = $request->get('title');
            $yt->url = env("YT_EMBED_URL") . $request->get('url') . "?autoplay=0";

            $yt->save();

            return response()->json('New Youtube link saved!', 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'message' => 'Something went wrong in YoutubeController.store'
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $user_id
     * @return \Illuminate\Http\Response
     */
    public function show(int $user_id)
    {
        try {

            $videoByUser = Youtube::where('user_id', $user_id)->get();

            return response()->json(['videos_by_user' => $videoByUser], 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'message' => 'Something went wrong in YoutubeController.show'
            ], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Youtube  $youtube
     * @return \Illuminate\Http\Response
     */
    public function edit(Youtube $youtube)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Youtube  $youtube
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Youtube $youtube)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        try {

            $yt = Youtube::findOrFail($id);
            $yt->delete();

            return response()->json('Video deleted!', 200);
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'message' => 'Something went wrong in YoutubeController.destroy'
            ], 400);
        }
    }
}
